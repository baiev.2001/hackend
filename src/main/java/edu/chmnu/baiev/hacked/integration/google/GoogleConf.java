package edu.chmnu.baiev.hacked.integration.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Configuration
public class GoogleConf {
    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
    private final String TOKENS_DIRECTORY_PATH = "tokens";
    private final String APPLICATION_NAME = "Quickstart";

    @Bean
    public HttpTransport httpTransport() {
        try {
            return GoogleNetHttpTransport.newTrustedTransport();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Bean
    public JacksonFactory jacksonFactory() {
        return JacksonFactory.getDefaultInstance();
    }

    @Bean
    public Credential credentialInterceptor(HttpTransport httpTransport, JacksonFactory jsonFactory) {
        try {
            File file = ResourceUtils.getFile("classpath:chrome/config.json");
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new FileReader(file));
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                    httpTransport, jsonFactory, clientSecrets, SCOPES)
                    .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                    .setAccessType("offline")
                    .build();
            LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();

            return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Bean
    public CustomRequestInitializer customRequestInitializer(Credential credentialInitialier) {
        return new CustomRequestInitializer(credentialInitialier);
    }

    @Bean
    public Drive drive(HttpTransport httpTransport, JacksonFactory jsonFactory, CustomRequestInitializer customRequestInitializer) {
        return new Drive.Builder(httpTransport, jsonFactory, customRequestInitializer)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public static class CustomRequestInitializer implements HttpRequestInitializer {
        private final HttpRequestInitializer chainInitializer;

        private CustomRequestInitializer(HttpRequestInitializer chainInitializer) {
            this.chainInitializer = chainInitializer;
        }

        @Override
        public void initialize(HttpRequest request) throws IOException {
            chainInitializer.initialize(request);
            request.setConnectTimeout(30_000);
            request.setReadTimeout(30_000);
        }
    }
}
