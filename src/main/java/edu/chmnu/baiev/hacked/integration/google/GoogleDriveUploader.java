package edu.chmnu.baiev.hacked.integration.google;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;

@Component
@Slf4j
public class GoogleDriveUploader {
    private static final String ROOT_FOLDER_ID = "1R763sMkGWkFaRBeSKbgdxhELPvx1tkRQ";

    private final Drive driveService;

    public GoogleDriveUploader(Drive driveService) {
        this.driveService = driveService;
    }

    public void upload(java.io.File filePath, String newName) throws IOException {
        File fileMetadata = new File();
        fileMetadata.setName(newName);
        fileMetadata.setParents(Collections.singletonList(ROOT_FOLDER_ID));
        FileContent mediaContent = new FileContent("image/jpeg", filePath);
        Drive.Files.Create createRequest = driveService.files().create(fileMetadata, mediaContent);
        createRequest.getMediaHttpUploader().setDirectUploadEnabled(true);
        createRequest.getMediaHttpUploader().setDisableGZipContent(true);
        File resp = createRequest.execute();
        log.info("File id:" + resp.getId());
    }
}