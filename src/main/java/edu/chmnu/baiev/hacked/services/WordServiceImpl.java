package edu.chmnu.baiev.hacked.services;

//import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static edu.chmnu.baiev.hacked.ProjectConstants.WORDS_PATH;

@Service
//@Slf4j
public class WordServiceImpl {

    private static List<String> WORDS = new ArrayList<>();
    private Integer thirdOfAmount;


    @PostConstruct
    public void initializeWords() {
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(new File(WORDS_PATH), "UTF-8");
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                WORDS.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
//            log.error("Words repository was failed to load full dict file");
        } finally {
            LineIterator.closeQuietly(lineIterator);
        }
        thirdOfAmount = WORDS.size() / 3;
    }

    public String getWord(Integer id) {
        return WORDS.get(id);
    }

    public Integer getAmount() {
        return WORDS.size();
    }

    public Integer getThirdOfAmount() {
        return thirdOfAmount;
    }
}
