package edu.chmnu.baiev.hacked.services.company;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyParser {
    private final EmailValidator emailValidator;

    public CompanyParser(EmailValidator emailValidator) {
        this.emailValidator = emailValidator;
    }

    public Company parse(String s) {
        String[] data = s.split(",");
        List<String> emails = new ArrayList<>(data.length - 1);
        for (int i = 1; i < data.length; ++i) {
            String email = data[i].trim();
            if(emailValidator.isValid(email)) {
                emails.add(email.trim());
            }
        }
        return Company
                .builder()
                .name(data[0].trim())
                .emailList(emails)
                .build();
    }
}
