package edu.chmnu.baiev.hacked.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
@Slf4j
public class PhotoWriter {

    /**
     * Writes text on an image
     *
     * @return boolean value, which means is input file was an image, or not
     */
    public Photo writeOnImage(String text, File file) {
        if (!file.exists()) {
            throw new RuntimeException("File does not exists");
        }
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(file);
            if (bufferedImage == null) {
                return null;
            }
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        Graphics graphics = bufferedImage.getGraphics();
        graphics.setColor(Color.RED);
        graphics.setFont(new Font("Arial Black", Font.BOLD, bufferedImage.getHeight() / 20));
        graphics.drawString(text, 30, bufferedImage.getWidth() / 3);
        try {
            ImageIO.write(bufferedImage, "jpg", file);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        log.info("Processed file " + file.getName());
        return new Photo(
                file,
                getRgb(bufferedImage)
        );
    }

    private Integer getRgb(BufferedImage bufferedImage) {
        return bufferedImage.getRGB(0, 0)
                + bufferedImage.getRGB(bufferedImage.getWidth() / 2, bufferedImage.getHeight() / 2)
                + bufferedImage.getRGB(bufferedImage.getWidth() - 1, bufferedImage.getHeight() - 1);
    }
}
