package edu.chmnu.baiev.hacked.services.company;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static edu.chmnu.baiev.hacked.ProjectConstants.COMPANY_EMAILS;

@Service
public class CompanyService {
    private static List<Company> COMPANIES = new ArrayList<>();

    private final CompanyParser companyParser;

    public CompanyService(CompanyParser companyParser) {
        this.companyParser = companyParser;
    }

    @PostConstruct
    public void initializeCompanies() {
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(new File(COMPANY_EMAILS), "UTF-8");
            while (lineIterator.hasNext()) {
                Company parse = companyParser.parse(lineIterator.nextLine());
                COMPANIES.add(parse);
            }
        } catch (IOException e) {
            e.printStackTrace();
//            log.error("Companies repository was failed to load full dict file");
        } finally {
            LineIterator.closeQuietly(lineIterator);
        }
    }

    public Company getCompany(Integer id) {
        return COMPANIES.get(id);
    }

    public Integer getAmount() {
        return COMPANIES.size();
    }

    public List<Company> getAll() {
        return COMPANIES;
    }
}
