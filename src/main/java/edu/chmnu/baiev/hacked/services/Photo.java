package edu.chmnu.baiev.hacked.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Photo {
    private File photoFile;

    private Integer rgbSum;
}
