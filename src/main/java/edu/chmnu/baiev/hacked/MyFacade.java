package edu.chmnu.baiev.hacked;

import edu.chmnu.baiev.hacked.integration.google.GoogleDriveUploader;
import edu.chmnu.baiev.hacked.services.Photo;
import edu.chmnu.baiev.hacked.services.PhotoWriter;
import edu.chmnu.baiev.hacked.services.WordServiceImpl;
import edu.chmnu.baiev.hacked.services.company.Company;
import edu.chmnu.baiev.hacked.services.company.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static edu.chmnu.baiev.hacked.ProjectConstants.PHOTO_PATH;

@Service
@Slf4j
public class MyFacade {
    //    private static final long SIZE_BOUND = 16_100_884_480L;
    private static final long SIZE_BOUND = 104_857_600L;

    private final GoogleDriveUploader driveUploader;
    private final WordServiceImpl wordService;
    private final PhotoWriter photoWriter;
    private final CompanyService companyService;


    public MyFacade(GoogleDriveUploader driveUploader,
                    WordServiceImpl wordService,
                    PhotoWriter photoWriter,
                    CompanyService companyService) {
        this.driveUploader = driveUploader;
        this.wordService = wordService;
        this.photoWriter = photoWriter;
        this.companyService = companyService;
    }

    public void doAction() {
        long startTime = System.currentTimeMillis();
//      Step 1. Remake photo with message
        File[] photoFilesArray = new File(PHOTO_PATH).listFiles();
        List<Photo> fileListContainer = StreamSupport.stream(Arrays.spliterator(photoFilesArray), true)
                .map(photoFile -> {
                    Collection<String> words = getWords();
                    return photoWriter.writeOnImage(String.join(" ", words), photoFile);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
//      Step 2. Send photos to google drive for all valid emails
        AtomicLong size = new AtomicLong(0);
        List<Company> companies = companyService.getAll();
        for (Company company : companies) {
            log.info(company.getName());
            fileListContainer.parallelStream()
                    .peek(photo -> {
                        boolean sent = false;
                        while (!sent) {
                            try {
                                driveUploader.upload(photo.getPhotoFile(), buildName(
                                        photo.getPhotoFile().getName(),
                                        company.getEmailList().get(0),
                                        photo.getRgbSum()));
                                sent = true;
                            } catch (IOException exception) {
                                exception.printStackTrace();
                            }
                        }
                    })
                    //              Take while whole size of photos less than 14gb 1035 mb.
                    .anyMatch((Photo photo) -> size.addAndGet(photo.getPhotoFile().length()) >= SIZE_BOUND);
            if (size.get() >= SIZE_BOUND) {
                break;
            }
        }
        long wastedTime = System.currentTimeMillis() - startTime;
        System.out.println("Time of work: " + wastedTime);
    }

    public Collection<String> getWords() {
        List<String> result = new ArrayList<>(5);
        result.add(wordService.getWord(5000));
        result.add(wordService.getWord(10000));
        result.add(wordService.getWord(15000));
        result.add(wordService.getWord(20000));
        result.add(wordService.getWord(25000));
        return result;
    }

    public String buildName(String oldName, String email, Integer rbgSum) {
        return new StringBuilder()
                .append(oldName)
                .append(email)
                .append(rbgSum.toString())
                .toString();
    }
}
