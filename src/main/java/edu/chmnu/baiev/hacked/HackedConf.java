package edu.chmnu.baiev.hacked;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HackedConf {

    @Bean
    public EmailValidator emailValidator() {
        return EmailValidator.getInstance();
    }
}
