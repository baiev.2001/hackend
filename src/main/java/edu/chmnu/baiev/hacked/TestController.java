package edu.chmnu.baiev.hacked;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    private final MyFacade facade;
    private boolean blocked = false;

    public TestController(MyFacade facade) {
        this.facade = facade;
    }


    @GetMapping("/test")
    public ResponseEntity<Object> startControl() {
        if (!blocked) {
            this.blocked = true;
            facade.doAction();
        }
        return ResponseEntity.ok().build();
    }

}
